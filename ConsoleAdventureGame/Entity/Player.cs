﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleAdventureGame
{
    public class Player
    {
        public static string Name { get; set; } = "Player";

        public static int HealthPoint { get; set; } = 100;

        public static int FullHealthPoint { get; set; } = 100;

        public static int AttackDamage { get; set; } = 25;

        public static int Level { get; set; } = 1;

        private static int Experience { get; set; } = 0;

        //Add exp to player and level upgrade
        public static void AddExp(int exp)
        {
            Experience += exp;

            if (Experience >= (Experience * Level))
            {
                int NextLevel = (Experience * Level);
                Experience -= NextLevel;

                Level++;
                int old_dmg = Player.AttackDamage;
                int old_hp = Player.HealthPoint;

                Player.HealthPoint += 50;
                Player.AttackDamage += 10;

                Player.FullHealthPoint += 50;

                Write(old_dmg, old_hp);

            }
        }

        private static void Write(int old_dmg, int old_hp)
        {
            Console.ForegroundColor = ConsoleColor.Magenta;
            Console.WriteLine("Level Upgrade!!!");
            Console.WriteLine("Your damage {0} -> {1} gain", old_dmg, Player.AttackDamage);
            Console.WriteLine("Your hp {0} -> {1} gain", old_hp, Player.HealthPoint);
            Console.ForegroundColor = ConsoleColor.White;
        }

    }
}
