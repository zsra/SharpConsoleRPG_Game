﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleAdventureGame.Map
{
    public class Hall
    {
        public static void EnterHall()
        {
            Console.ForegroundColor = ConsoleColor.Magenta;
            Console.WriteLine("Welcome in Hall!");
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("Choose Upgrade");
            Console.WriteLine("1 - Attack Damage will be increase by 200%");
            Console.WriteLine("2 - HP will be increase by 300%");
            Console.Write("Choose one:\t");
            int input = int.Parse(Console.ReadLine());
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Go to new level!");
            Console.ForegroundColor = ConsoleColor.White;

            if (input.Equals(1)) Player.AttackDamage += Player.AttackDamage;
            if (input.Equals(2))
            {
                Player.HealthPoint += (Player.HealthPoint * 2);
                Player.FullHealthPoint += (Player.HealthPoint * 2);
            }

            Player.HealthPoint = Player.FullHealthPoint;

        }
    }
}
