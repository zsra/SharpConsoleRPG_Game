﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleAdventureGame.Map
{
    public class LevelBuilder
    {
        public static int Level = 1;
        private const int UPGRADE_LEVEL_NUMBER = 10;
        private static readonly int Size = Level * UPGRADE_LEVEL_NUMBER;
        private const int MAX_ENEMIES_NUMBER = 5;
        public static int[] Map = new int[Size];
        private static readonly Random rnd = new Random();

        private static int GetRandomSpotToEnemy()
        {
            lock (rnd)
            {
                return rnd.Next(1, Size - 1);
            }
        }

        private static int GetRandomEnemiesNumber()
        {
            lock (rnd)
            {
                return rnd.Next(1, MAX_ENEMIES_NUMBER);
            }
        }

        private static int EnemiesNumberOnMap(ref int lvl) => lvl * GetRandomEnemiesNumber();

        public static void BuildMap()
        {
            int EnemyNumber = EnemiesNumberOnMap(ref Level);
            int EnemySpot = GetRandomSpotToEnemy();
            
            for(int number = EnemyNumber; number > 0; number--)
            {
                for (int index = 0; index < Size - 1; index++)
                {

                    if (EnemySpot.Equals(index))
                    {
                        Map[index] = 1;
                        EnemySpot = GetRandomSpotToEnemy();
                        break;
                    }
                }
            }

            Map[Size - 1] = 2;
                        
        }
        
    }
}
